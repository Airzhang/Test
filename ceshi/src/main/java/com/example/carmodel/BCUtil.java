package com.example.carmodel;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * @Author zhangxin
 * @Date 2022/6/17 0:28
 */
public class BCUtil {

    public static<T> T ptff(T t){
        Class<?> aClass = t.getClass();
        try {
            Method setCreateBy = aClass.getMethod("setCreateBy", String.class);
            setCreateBy.invoke(t,"admin");
            Method setUpdateBy = aClass.getMethod("setUpdateBy", String.class);
            setUpdateBy.invoke(t,"admin");

            Method setCreateName = aClass.getMethod("setCreateName", String.class);
            setCreateName.invoke(t,"msfl");
            Method setUpdateName = aClass.getMethod("setUpdateName", String.class);
            setUpdateName.invoke(t,"msfl");


            Method setCreateTime = aClass.getMethod("setCreateTime", Date.class);
            setCreateTime.invoke(t,new Date());
            Method setUpdateTime = aClass.getMethod("setUpdateTime", Date.class);
            setUpdateTime.invoke(t,new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    public static<T> T ptff2(T t,String modelId){
        Class<?> aClass = t.getClass();
        try {
            Method setCreateBy = aClass.getMethod("setCreateBy", String.class);
            setCreateBy.invoke(t,"admin");
            Method setUpdateBy = aClass.getMethod("setUpdateBy", String.class);
            setUpdateBy.invoke(t,"admin");

            Method setCreateName = aClass.getMethod("setCreateName", String.class);
            setCreateName.invoke(t,"msfl");
            Method setUpdateName = aClass.getMethod("setUpdateName", String.class);
            setUpdateName.invoke(t,"msfl");

            Method setCreateTime = aClass.getMethod("setCreateTime", Date.class);
            setCreateTime.invoke(t,new Date());
            Method setUpdateTime = aClass.getMethod("setUpdateTime", Date.class);
            setUpdateTime.invoke(t,new Date());
            Method setDeviceId = aClass.getMethod("setDeviceId", String.class);
            setDeviceId.invoke(t,modelId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }
}
