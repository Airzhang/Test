package com.example.carmodel;


import com.example.carmodel.po.*;

import java.util.List;

/**
 * @Author zhangxin
 * @Date 2022/6/20 13:44
 */
public interface ABCMapper {
    void saveBatchModel(List<VehicleModel> aa);
    void saveBatchModel2(List<VehicleEngine> aa2);
    void saveBatchModel3(List<VehicleFull> aa3);
    void saveBatchModel4(List<VehicleCab> aa4);
    void saveBatchModel5(List<VehicleGearbox> aa5);
    void saveBatchModel6(List<VehicleTyre> aa6);
    void saveBatchModel7(List<VehicleFuel> aa7);
    void saveBatchModel8(List<VehicleChassis> aa8);
    void saveBatchModel9(List<VehicleInconfig> aa9);
    void saveBatchModel10(List<VehicleCell> aa10);
    void saveBatchModel11(List<VehicleTopconfig> aa11);
}
