package com.example.carmodel;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author zhangxin
 * @Date 2022/6/16 23:30
 */

@Getter
@Setter
public class VehicleModelFull {
    //    基本信息
    private String trailerSign;
    private String deviceNum;

//    private String threeModelId;

//    private String deviceCode;
    private String categoryName;
    private String categoryNameParent;





    private String logoName;


    private String seriesName;
    private String useOrNot;

//    private BigDecimal price;


//    private String modelStatus;



/*

    private String driveType;


    private BigDecimal carBodyLength;


    private BigDecimal carBodyWidth;


    private BigDecimal carBodyHeight;


    private BigDecimal fullLoadWeight;


    private BigDecimal maxCarSpeed;


    private String weightType;


    private BigDecimal standardWeight;



    //    发动机

    private String engineModel;


    private String engineBrand;


    private String fuelType;


    private BigDecimal displacement;


    private String emissionStandard;


    private BigDecimal maxHorsepower;


    private BigDecimal maxPower;
    //    整车参数

    private BigDecimal pullWeight;


    private BigDecimal containerLength;


    private BigDecimal containerWidth;


    private BigDecimal containerHeight;


    private String wheelBase;


    private String containerModel;


    private Date listedDate;


    private BigDecimal standardEndurance;
    //    驾驶室参数

    private String cabModel;


    private String cabNumber;


    private String seatNumber;

    private String gearboxBrand;


    private String gearboxModel;


    private String shiftWay;
    //    轮胎

    private BigDecimal tyreNumber;


    private String tyreModel;
    //    油箱

    private String fuelTexture;


    private BigDecimal fuelCapacity;
    //    底盘

    private String chassisModel;


    private String chassisBrand;


    private BigDecimal axisNumber;
    //    内部配置
    private String seatNumber2;
    //    电池
    private String cellType;


    private String cellBrand;
    //    上装参数
    private BigDecimal stirCubage;*/
    //车型ID

    private String deviceId;
}
