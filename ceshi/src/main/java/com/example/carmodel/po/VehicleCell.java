package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleCell extends BaseObject{



    

    private String cellId;


    private String deviceId;


    private String cellType;


    private String cellAh;


    private BigDecimal enduranceMileage;


    private String topPower;


    private String cellBrand;


    private String cellModel;



}
