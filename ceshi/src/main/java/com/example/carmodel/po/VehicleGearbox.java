package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter
public class VehicleGearbox extends BaseObject{



    

    private String gearboxId;


    private String deviceId;


    private String gearboxBrand;


    private String gearboxModel;


    private String shiftWay;
}
