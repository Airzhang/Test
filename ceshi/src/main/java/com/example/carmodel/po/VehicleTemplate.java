package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleTemplate extends BaseObject{



    

    private String templateId;


    private String categoryId;


    private String categoryName;


    private String carControlInfo;


    private String carPageInfo;


    private Integer modelVersion;


    private Integer modelConfigCount;

}
