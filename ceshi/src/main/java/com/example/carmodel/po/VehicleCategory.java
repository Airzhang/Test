package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleCategory extends BaseObject{



    

    private String categoryId;


    private String categoryName;


    private String parentId;


    private String code;



}
