package com.example.carmodel.po;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.example.carmodel.CommonDictionaries;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleModel extends BaseObject {


    private String deviceId;


    private String seriesId;


    private String manufacturerId;


    private String manufacturerName;


    private String logoId;


    private String deviceCode;


    private String deviceName;


    private String deviceTypeCode;


    private String deviceNum;


    private BigDecimal price;


    private String remark;


    private String auditStatus;


    private String useOrNot;


    private String modelStatus;


    private String trailerSign;


    private String categoryId;


    private String driveType;


    private BigDecimal carBodyLength;


    private BigDecimal carBodyWidth;


    private BigDecimal carBodyHeight;


    private BigDecimal fullLoadWeight;


    private BigDecimal maxCarSpeed;


    private String weightType;


    private String templateId;


    private String topConfigBrand;


    private BigDecimal standardWeight;


    private String seriesName;


    private String categoryName;


    private String categoryNameParent;


    private String versions;


    private String priceType;


    private String logoName;


    private String threeModelId;

    /**
     * 按规则生成车型名称
     *
     * @param vehicleModelDto
     * @return
     */
    public String createVehicleModelName(VehicleModelDto vehicleModelDto, VehicleCategory vehicleCategory) {
        StringBuilder vehicleModelName = new StringBuilder();
        vehicleModelDto.setVehicleEngine(new VehicleEngine());
        if (vehicleCategory == null || vehicleModelDto.getVehicleEngine() == null) {
            return null;
        }
        //挂车
        //品牌+车身长度（米）+额定载重（吨）+轴数（轴）+车辆用途+公告型号
        if ("挂车".equals(vehicleModelDto.getVehInfoDto().getCategoryNameParent())) {
            if (vehicleModelDto.getVehInfoDto() != null) {
                vehicleModelName.append(vehicleModelDto.getVehInfoDto().getLogoName()+" ");
                if (vehicleModelDto.getVehInfoDto().getCarBodyLength() != null) {
                    BigDecimal mi = new BigDecimal("1000");
                    vehicleModelName.append(vehicleModelDto.getVehInfoDto().getCarBodyLength().stripTrailingZeros().divide(mi).toPlainString() + "米" + " ");
                }
                if (vehicleModelDto.getVehInfoDto().getStandardWeight() != null) {
                    vehicleModelName.append(vehicleModelDto.getVehInfoDto().getStandardWeight().stripTrailingZeros().toPlainString()+"吨" + " ");
                }
            }
            if (vehicleModelDto.getVehicleChassis() != null) {
                if (vehicleModelDto.getVehicleChassis().getAxisNumber() != null) {
                    vehicleModelName.append(vehicleModelDto.getVehicleChassis().getAxisNumber().stripTrailingZeros().toPlainString()+"轴" + " ");
                }
            }
            vehicleModelName.append(vehicleCategory.getCategoryName() + " ");
            if (vehicleModelDto.getVehInfoDto() != null) {
                if(StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDeviceNum())){
                    vehicleModelName.append("(" + vehicleModelDto.getVehInfoDto().getDeviceNum() + ")");
                }
            }
            String[] nulls = vehicleModelName.toString().split("null");
            String vn = ArrayUtil.join(nulls, "");
            return vn;
        }

        //非挂车
        switch (vehicleCategory.getCategoryName()) {
            case "客车":
            case "新能源客车":
            case "校车":
            case "公交":
                if (CommonDictionaries.FuelTypeDic.FTD2.getCode().equals(vehicleModelDto.getVehicleEngine().getFuelType())) {
                    //纯电动
                    //车系+发动机品牌+马力+发动机+排放标准+变速箱品牌+驱动形式+电池容量+座位数+车辆用途+纯电动+公告型号
                    if (vehicleModelDto.getVehInfoDto() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehInfoDto().getSeriesName() + " ");
                    }
                    if (vehicleModelDto.getVehicleEngine() != null) {
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineBrand())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineBrand() + ")" + " ");
                        }
                        if (vehicleModelDto.getVehicleEngine().getMaxHorsepower() != null) {
                            vehicleModelName.append(vehicleModelDto.getVehicleEngine().getMaxHorsepower().stripTrailingZeros().toPlainString() + "马力" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineModel())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineModel() + ")" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEmissionStandard())) {
                            vehicleModelName.append("(" + CommonDictionaries.EmissionStandardDic.getDescValue(vehicleModelDto.getVehicleEngine().getEmissionStandard()) + ")" + " ");
                        }
                    }
                    if (vehicleModelDto.getVehicleGearbox() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleGearbox().getGearboxBrand())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehicleGearbox().getGearboxBrand() + ")" + " ");
                    }
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDriveType())) {
                        vehicleModelName.append(CommonDictionaries.DriveTypeDic.getDescValue(vehicleModelDto.getVehInfoDto().getDriveType()) + " ");
                    }
                    if (vehicleModelDto.getVehicleCell() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCell().getCellAh())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleCell().getCellAh() + "Kwh" + " ");
                    }
                    if (vehicleModelDto.getVehicleInconfig() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleInconfig().getSeatNumber())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleInconfig().getSeatNumber() + "座" + " ");
                    }
                    vehicleModelName.append(vehicleCategory.getCategoryName() + " ");
                    vehicleModelName.append(CommonDictionaries.FuelTypeDic.FTD2.getDesc() + " ");
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDeviceNum())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehInfoDto().getDeviceNum() + ")");
                    }
                } else {
                    //非纯电动
                    //车系+发动机品牌+马力+发动机+排放标准+变速箱品牌+驱动形式+电池容量+座位数+车辆用途+公告型号
                    if (vehicleModelDto.getVehInfoDto() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehInfoDto().getSeriesName() + " ");
                    }
                    if (vehicleModelDto.getVehicleEngine() != null) {
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineBrand())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineBrand() + ")" + " ");
                        }
                        if (vehicleModelDto.getVehicleEngine().getMaxHorsepower() != null) {
                            vehicleModelName.append(vehicleModelDto.getVehicleEngine().getMaxHorsepower().stripTrailingZeros().toPlainString() + "马力" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineModel())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineModel() + ")" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEmissionStandard())) {
                            vehicleModelName.append("(" + CommonDictionaries.EmissionStandardDic.getDescValue(vehicleModelDto.getVehicleEngine().getEmissionStandard()) + ")" + " ");
                        }
                    }
                    if (vehicleModelDto.getVehicleGearbox() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleGearbox().getGearboxBrand())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehicleGearbox().getGearboxBrand() + ")" + " ");
                    }
                    if (vehicleModelDto.getVehInfoDto() != null &&  StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDriveType())) {
                        vehicleModelName.append(CommonDictionaries.DriveTypeDic.getDescValue(vehicleModelDto.getVehInfoDto().getDriveType()) + " ");
                    }
                    if (vehicleModelDto.getVehicleCell() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCell().getCellAh())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleCell().getCellAh() + "Kwh" + " ");
                    }
                    if (vehicleModelDto.getVehicleInconfig() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleInconfig().getSeatNumber())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleInconfig().getSeatNumber() + "座" + " ");
                    }
                    vehicleModelName.append(vehicleCategory.getCategoryName() + " ");
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDeviceNum())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehInfoDto().getDeviceNum() + ")");
                    }
                }
                break;
            default:
                if (CommonDictionaries.FuelTypeDic.FTD2.getCode().equals(vehicleModelDto.getVehicleEngine().getFuelType())) {
                    //纯电动
                    //车系+发动机品牌+马力+发动机+排放标准+变速箱品牌+驱动形式+货箱长度+搅拌容积+电池容量+座位排数+货箱形式+车辆用途+纯电动+公告型号
                    if (vehicleModelDto.getVehInfoDto() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehInfoDto().getSeriesName() + " ");
                    }
                    if (vehicleModelDto.getVehicleEngine() != null) {
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineBrand())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineBrand() + ")" + " ");
                        }
                        if (vehicleModelDto.getVehicleEngine().getMaxHorsepower() != null) {
                            vehicleModelName.append(vehicleModelDto.getVehicleEngine().getMaxHorsepower().stripTrailingZeros().toPlainString() + "马力" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineModel())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineModel() + ")" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEmissionStandard())) {
                            vehicleModelName.append("(" + CommonDictionaries.EmissionStandardDic.getDescValue(vehicleModelDto.getVehicleEngine().getEmissionStandard()) + ")" + " ");
                        }
                    }
                    if (vehicleModelDto.getVehicleGearbox() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleGearbox().getGearboxBrand())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehicleGearbox().getGearboxBrand() + ")" + " ");
                    }
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDriveType())) {
                        vehicleModelName.append(CommonDictionaries.DriveTypeDic.getDescValue(vehicleModelDto.getVehInfoDto().getDriveType()) + " ");
                    }
                    if (vehicleModelDto.getVehicleFull() != null && vehicleModelDto.getVehicleFull().getContainerLength() != null) {
                        BigDecimal mi = new BigDecimal("1000");
                        vehicleModelName.append(vehicleModelDto.getVehicleFull().getContainerLength().stripTrailingZeros().divide(mi).toPlainString() + "米" + " ");
                    }
                    if (vehicleModelDto.getVehicleTopconfig() != null && vehicleModelDto.getVehicleTopconfig().getStirCubage() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehicleTopconfig().getStirCubage().stripTrailingZeros().toPlainString() + "立方" + " ");
                    }
                    if (vehicleModelDto.getVehicleCell() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCell().getCellAh())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleCell().getCellAh() + "Kwh" + " ");
                    }
                    if (vehicleModelDto.getVehicleCab() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCab().getSeatNumber())) {
                        vehicleModelName.append(CommonDictionaries.SeatNumberDic.getDescValue(vehicleModelDto.getVehicleCab().getSeatNumber()) + " ");
                    }
                    if (vehicleModelDto.getVehicleFull() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleFull().getContainerModel())) {
                        vehicleModelName.append(CommonDictionaries.ContainerModelDic.getDescValue(vehicleModelDto.getVehicleFull().getContainerModel()) + " ");
                    }
                    vehicleModelName.append(vehicleCategory.getCategoryName() + " ");
                    vehicleModelName.append(CommonDictionaries.FuelTypeDic.FTD2.getDesc() + " ");
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDeviceNum())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehInfoDto().getDeviceNum() + ")");
                    }
                } else {
                    //非纯电动
                    //车系+发动机品牌+马力+发动机+排放标准+变速箱品牌+驱动形式+货箱长度+搅拌容积+电池容量+座位排数+货箱形式+车辆用途+公告型号
                    if (vehicleModelDto.getVehInfoDto() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehInfoDto().getSeriesName() + " ");
                    }
                    if (vehicleModelDto.getVehicleEngine() != null) {
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineBrand())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineBrand() + ")" + " ");
                        }
                        if (vehicleModelDto.getVehicleEngine().getMaxHorsepower() != null) {
                            vehicleModelName.append(vehicleModelDto.getVehicleEngine().getMaxHorsepower().stripTrailingZeros().toPlainString() + "马力" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEngineModel())) {
                            vehicleModelName.append("(" + vehicleModelDto.getVehicleEngine().getEngineModel() + ")" + " ");
                        }
                        if (StrUtil.isNotEmpty(vehicleModelDto.getVehicleEngine().getEmissionStandard())) {
                            vehicleModelName.append("(" + CommonDictionaries.EmissionStandardDic.getDescValue(vehicleModelDto.getVehicleEngine().getEmissionStandard()) + ")" + " ");
                        }
                    }
                    if (vehicleModelDto.getVehicleGearbox() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleGearbox().getGearboxBrand())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehicleGearbox().getGearboxBrand() + ")" + " ");
                    }
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDriveType())) {
                        vehicleModelName.append(CommonDictionaries.DriveTypeDic.getDescValue(vehicleModelDto.getVehInfoDto().getDriveType()) + " ");
                    }
                    if (vehicleModelDto.getVehicleFull() != null && vehicleModelDto.getVehicleFull().getContainerLength() != null) {
                        BigDecimal mi = new BigDecimal("1000");
                        vehicleModelName.append(vehicleModelDto.getVehicleFull().getContainerLength().stripTrailingZeros().divide(mi).toPlainString() + "米" + " ");
                    }
                    if (vehicleModelDto.getVehicleTopconfig() != null && vehicleModelDto.getVehicleTopconfig().getStirCubage() != null) {
                        vehicleModelName.append(vehicleModelDto.getVehicleTopconfig().getStirCubage().stripTrailingZeros().toPlainString() + "立方" + " ");
                    }
                    if (vehicleModelDto.getVehicleCell() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCell().getCellAh())) {
                        vehicleModelName.append(vehicleModelDto.getVehicleCell().getCellAh() + "Kwh" + " ");
                    }
                    if (vehicleModelDto.getVehicleCab() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleCab().getSeatNumber())) {
                        vehicleModelName.append(CommonDictionaries.SeatNumberDic.getDescValue(vehicleModelDto.getVehicleCab().getSeatNumber()) + " ");
                    }
                    if (vehicleModelDto.getVehicleFull() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehicleFull().getContainerModel())) {
                        vehicleModelName.append(CommonDictionaries.ContainerModelDic.getDescValue(vehicleModelDto.getVehicleFull().getContainerModel()) + " ");
                    }
                    vehicleModelName.append(vehicleCategory.getCategoryName() + " ");
                    if (vehicleModelDto.getVehInfoDto() != null && StrUtil.isNotEmpty(vehicleModelDto.getVehInfoDto().getDeviceNum())) {
                        vehicleModelName.append("(" + vehicleModelDto.getVehInfoDto().getDeviceNum() + ")");
                    }
                }
        }
        String[] nulls = vehicleModelName.toString().split("null");
        String vn = ArrayUtil.join(nulls, "");
        return vn;
    }
}
