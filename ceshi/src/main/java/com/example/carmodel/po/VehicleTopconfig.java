package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter
public class VehicleTopconfig extends BaseObject{


    

    private String topConfigId;


    private String deviceId;


    private BigDecimal boomVerticalHeight;


    private BigDecimal boomLevelLength;


    private BigDecimal boomVerticalDepth;


    private BigDecimal minSpreadLength;


    private BigDecimal cranePitchNumber;


    private String highChair;


    private BigDecimal firstBoomLength;


    private BigDecimal firstBoomCorner;


    private BigDecimal secondBoomLength;


    private BigDecimal secondBoomCorner;


    private BigDecimal thirdBoomLength;


    private BigDecimal thirdBoomCorner;


    private BigDecimal fourthBoomLength;


    private BigDecimal fourthBoomCorner;


    private BigDecimal fifthBoomLength;


    private BigDecimal fifthBoomCorner;


    private BigDecimal turntableCorner;


    private BigDecimal sixthBoomLength;


    private BigDecimal sixthBoomCorner;


    private BigDecimal craneMaxHeight;


    private String craneModel;


    private String craneWeight;


    private BigDecimal stirCubage;


    private String collMachineBrand;


    private String collMachineModel;


    private String craneBrand;


    private String transportMedium;


    private String laterSupportingLeg;


    private String beforeSupportingLeg;


    private BigDecimal tankCubage;


    private BigDecimal tankStorehouseNumber;


    private BigDecimal supportingLegNumber;


    private BigDecimal maxPullQuality;


    private String oilCylinderBrand;


    private String liftDevice;

    private String hydraumaticLadderStand;


    private String coldboxGuideRail;


    private String coldboxMeetHook;


    private BigDecimal coldboxMeetHookNumber;


    private String coldboxBrand;


    private String axleNumber;


    private String axleBrand;


    private String sweepWidth;


    private String sweepSpeed;


    private String spreaderActiveArea;


    private String spreaderWidth;


    private String spreaderDensity;


    private String tankMaterial;


    private String motor;


    private String reducer;


    private String oilPump;


    private String dustbinCubage;


    private String sweepArea;


    private String sweepPavementDevice;


    private BigDecimal sweepDiskNumber;


    private String highPressurePump;


    private String sewagePumpBrand;


    private String rearOutrigger;


    private BigDecimal balanceLegsNumber;


    private BigDecimal craneWeightOwn;


    private String rotaryBackStructure;


    private BigDecimal maxWorkingHeight;


    private BigDecimal maxWorkingRadius;

    private BigDecimal rotationAngle;

}
