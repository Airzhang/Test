package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleTyre extends BaseObject{



    

    private String tyreId;


    private String deviceId;


    private BigDecimal tyreNumber;


    private String tyreModel;




}
