package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleCab extends BaseObject{


    

    private String cabId;


    private String deviceId;


    private String cabModel;


    private String cabNumber;


    private String seatNumber;

}
