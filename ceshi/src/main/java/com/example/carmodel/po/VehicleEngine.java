package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleEngine  extends BaseObject{



    

    private String engineId;


    private String deviceId;


    private String engineModel;


    private String engineBrand;


    private BigDecimal cylinderNumber;


    private String fuelType;


    private BigDecimal displacement;


    private String emissionStandard;


    private BigDecimal maxHorsepower;


    private BigDecimal maxPower;


    private String engineBraking;
}
