package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleControl extends BaseObject{



    

    private String controlId;

    private String deviceId;


    private String beforeBrakeType;


    private String afterBrakeType;


    private String hydraulicRetarder;


    private String upDownHang;
}
