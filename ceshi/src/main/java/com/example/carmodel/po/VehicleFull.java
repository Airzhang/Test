package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter
public class VehicleFull extends BaseObject{



    private String fullId;


    private String deviceId;


    private BigDecimal ratedWeight;


    private BigDecimal fullWeight;


    private BigDecimal pullWeight;


    private BigDecimal containerLength;


    private BigDecimal containerWidth;


    private BigDecimal containerHeight;


    private String wheelBase;


    private String fairing;


    private String containerModel;


    private String containerTexture;


    private Date listedDate;


    private BigDecimal fuelConsumption;


    private BigDecimal standardEndurance;


    private BigDecimal containerChassisThicknes;


    private BigDecimal containerBoardThicknes;


    private String rolledSteelModel;
}
