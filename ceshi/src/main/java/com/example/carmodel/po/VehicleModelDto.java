package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-04-26
 */
@Getter
@Setter
public class VehicleModelDto{

    private VehInfoDto vehInfoDto;


    private VehicleCab vehicleCab;

    private VehicleCell vehicleCell;

    private VehicleChassis vehicleChassis;

    private VehicleControl vehicleControl;

    private VehicleEngine vehicleEngine;

    private VehicleFuel vehicleFuel;

    private VehicleFull vehicleFull;

    private VehicleGearbox vehicleGearbox;

    private VehicleInconfig vehicleInconfig;

    private VehicleMotor vehicleMotor;

    private VehicleOutconfig vehicleOutconfig;

    private VehicleTopconfig vehicleTopconfig;

    private VehicleTyre vehicleTyre;

}
