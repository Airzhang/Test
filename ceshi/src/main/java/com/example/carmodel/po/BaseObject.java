package com.example.carmodel.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Description:
 * Author:   admin
 * Date:     2019-07-19 10:05
 */
@Getter
@Setter
public  class BaseObject {

    private String createBy;

    private Date createTime;


    private String updateBy;

    private Date updateTime;

    private String createName;
    private String updateName;

    private Boolean validInd=true;
}
