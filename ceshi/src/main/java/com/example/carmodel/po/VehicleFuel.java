package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleFuel extends BaseObject{



    

    private String fuelId;


    private String deviceId;


    private String fuelTexture;


    private BigDecimal fuelCapacity;


    private BigDecimal airbottleCapacity;
}
