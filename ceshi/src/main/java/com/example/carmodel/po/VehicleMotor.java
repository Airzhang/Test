package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleMotor extends BaseObject{



    

    private String motorId;


    private String deviceId;


    private String motorBrand;


    private String motorModel;


    private BigDecimal topPower;


    private BigDecimal ratedPower;




}
