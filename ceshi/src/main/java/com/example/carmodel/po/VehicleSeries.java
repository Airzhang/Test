package com.example.carmodel.po;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

@ExcelIgnoreUnannotated
public class VehicleSeries extends BaseObject{



    

    private String seriesId;


    private String logoId;

    @ExcelProperty("seriesName")

    private String seriesName;

    @ExcelProperty("logoName")

    private String logoName;

    @ExcelProperty("threeSeriesId")

    private String threeSeriesId;

    @ExcelProperty("threeBrandId")

    private String threeBrandId;
}
