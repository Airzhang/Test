package com.example.carmodel.po;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter
@ExcelIgnoreUnannotated
public class VehicleLogo extends BaseObject{



    

    private String logoId;

    @ExcelProperty("logoName")

    private String logoName;


    private String brandClassification;


    private String logoImage;

    @ExcelProperty("firstLetter")

    private String firstLetter;

    @ExcelProperty("trailerSign")

    private String trailerSign;

    @ExcelProperty("threeBrandId")

    private String threeBrandId;

}
