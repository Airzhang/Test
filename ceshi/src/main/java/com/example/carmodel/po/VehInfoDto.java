package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @Author zhangxin
 * @Date 2022/5/26 1:51
 */
@Getter
@Setter
public class VehInfoDto{
    private String categoryName;
    private String categoryNameParent;
    private String deviceId;

    private String seriesId;

    private String seriesName;

    private String manufacturerId;

    private String manufacturerName;

    private String logoId;

    private String deviceCode;

    private String deviceName;

    private String deviceTypeCode;

    private String deviceNum;

    private BigDecimal price;

    private String remark;

    private String auditStatus;

    private String useOrNot;


    private String modelStatus;

    private Boolean validInd;

    private String usingStatus;

    private String trailerSign;

    private String categoryId;

    private String driveType;

    private BigDecimal carBodyLength;

    private BigDecimal carBodyWidth;

    private BigDecimal carBodyHeight;

    private BigDecimal fullLoadWeight;

    private BigDecimal maxCarSpeed;

    private String weightType;

    private String templateId;

    private String topConfigBrand;

    private BigDecimal standardWeight;

    private String createTime;

    private String createBy;

    private String updateTime;

    private String updateBy;

    private String logoName;

    private String versions;

    private String priceType;

    private String createName;
    private String updateName;
}
