package com.example.carmodel.po;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangxin
 * @since 2022-05-19
 */
@Getter
@Setter

public class VehicleChassis extends BaseObject{



    

    private String chassisId;


    private String deviceId;


    private String chassisModel;


    private String chassisBrand;


    private BigDecimal axisNumber;

}
