package com.example.carmodel;

import cn.hutool.core.io.FileUtil;
import com.alibaba.excel.EasyExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhangxin
 * @Date 2022/6/20 21:26
 */
@RestController
public class AA {

    @Autowired
    private ABC abc;

    @RequestMapping("/aa")
    public void aa(){
        String a = "C:\\zxwork\\data\\VehicleModelDto.xlsx";
        EasyExcel.read(FileUtil.file(a)
                , VehicleModelFull.class
                , new VehicleModelListener(abc)).sheet().doRead();
    }
}
