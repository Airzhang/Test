
package com.example.carmodel;

/**
 * Purpose.
 *
 * A description of why this class exists.  
 *   For what reason was it written?  
 *   Which jobs does it perform?
 * {@code DataAccessException} using 
 * @author how
 * @date 2021-09-16
 */
public class IdGen {
    private volatile static IdGenerator basicEntityIdGenerator;
    public static String uuid()  {
        String s = "";
        synchronized (s) {
                return IdGen.getInstance().generator();

        }
    }

    public static IdGenerator getInstance() {
        if (basicEntityIdGenerator == null) {
            synchronized (IdGenerator.class) {
                if (basicEntityIdGenerator == null) basicEntityIdGenerator = new IdGenerator();
            }
        }
        return basicEntityIdGenerator;
    }
}
