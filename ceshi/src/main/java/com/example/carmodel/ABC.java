package com.example.carmodel;

import cn.hutool.core.bean.BeanUtil;
import com.example.carmodel.po.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhangxin
 * @Date 2022/6/20 10:32
 */
@Repository
public class ABC {
    //基本信息
    @Autowired
    private ABCMapper abcMapper;

    public void saveModel(List<VehicleModelFull> cachedDataList){
        //数据集合
        List<VehicleModel> aa = new ArrayList<>();
        List<VehicleEngine> aa2 = new ArrayList<>();
        List<VehicleFull> aa3 = new ArrayList<>();
        List<VehicleCab> aa4 = new ArrayList<>();
        List<VehicleGearbox> aa5 = new ArrayList<>();
        List<VehicleTyre> aa6 = new ArrayList<>();
        List<VehicleFuel> aa7 = new ArrayList<>();
        List<VehicleChassis> aa8 = new ArrayList<>();
        List<VehicleInconfig> aa9 = new ArrayList<>();
        List<VehicleCell> aa10 = new ArrayList<>();
        List<VehicleTopconfig> aa11 = new ArrayList<>();
        //数据处理
        for (VehicleModelFull vehicleModelFull : cachedDataList) {
            //基本信息
            VehicleModel vehicleModel = BeanUtil.copyProperties(vehicleModelFull, VehicleModel.class);
            vehicleModel.setDeviceId(IdGen.uuid());
            BCUtil.ptff(vehicleModel);


           /* //发动机
            VehicleEngine vehicleEngine = BeanUtil.copyProperties(vehicleModelFull, VehicleEngine.class);
            vehicleEngine.setEngineId(IdGen.uuid());
            //发动机排量保留1位小数，四舍五入
            if(vehicleEngine.getDisplacement() != null){
                BigDecimal bigDecimal = vehicleEngine.getDisplacement().setScale(1, RoundingMode.HALF_UP);
                vehicleEngine.setDisplacement(bigDecimal);
            }
            BCUtil.ptff2(vehicleEngine, vehicleModel.getDeviceId());
            aa2.add(vehicleEngine);
            //整车参数
            VehicleFull vehicleFull = BeanUtil.copyProperties(vehicleModelFull, VehicleFull.class);
            vehicleFull.setFullId(IdGen.uuid());
            BCUtil.ptff2(vehicleFull, vehicleModel.getDeviceId());
            aa3.add(vehicleFull);
            //驾驶室参数
            VehicleCab vehicleCab = BeanUtil.copyProperties(vehicleModelFull, VehicleCab.class);
            vehicleCab.setCabId(IdGen.uuid());
            BCUtil.ptff2(vehicleCab, vehicleModel.getDeviceId());
            aa4.add(vehicleCab);
            //变速箱
            VehicleGearbox vehicleGearbox = BeanUtil.copyProperties(vehicleModelFull, VehicleGearbox.class);
            vehicleGearbox.setGearboxId(IdGen.uuid());
            BCUtil.ptff2(vehicleGearbox, vehicleModel.getDeviceId());
            aa5.add(vehicleGearbox);
            //轮胎
            VehicleTyre vehicleTyre = BeanUtil.copyProperties(vehicleModelFull, VehicleTyre.class);
            vehicleTyre.setTyreId(IdGen.uuid());
            BCUtil.ptff2(vehicleTyre, vehicleModel.getDeviceId());
            aa6.add(vehicleTyre);
            //油箱
            VehicleFuel vehicleFuel = BeanUtil.copyProperties(vehicleModelFull, VehicleFuel.class);
            vehicleFuel.setFuelId(IdGen.uuid());
            BCUtil.ptff2(vehicleFuel, vehicleModel.getDeviceId());
            aa7.add(vehicleFuel);
            //底盘
            VehicleChassis vehicleChassis = BeanUtil.copyProperties(vehicleModelFull, VehicleChassis.class);
            vehicleChassis.setChassisId(IdGen.uuid());
            BCUtil.ptff2(vehicleChassis, vehicleModel.getDeviceId());
            aa8.add(vehicleChassis);
            //内部配置
            VehicleInconfig vehicleInconfig = new VehicleInconfig();
            vehicleInconfig.setInConfigId(IdGen.uuid());
            vehicleInconfig.setSeatNumber(vehicleModelFull.getSeatNumber2());
            BCUtil.ptff2(vehicleInconfig, vehicleModel.getDeviceId());
            aa9.add(vehicleInconfig);
            //电池
            VehicleCell vehicleCell = BeanUtil.copyProperties(vehicleModelFull, VehicleCell.class);
            vehicleCell.setCellId(IdGen.uuid());
            BCUtil.ptff2(vehicleCell, vehicleModel.getDeviceId());
            aa10.add(vehicleCell);
            //上装参数
            VehicleTopconfig vehicleTopconfig = BeanUtil.copyProperties(vehicleModelFull, VehicleTopconfig.class);
            vehicleTopconfig.setTopConfigId(IdGen.uuid());
            BCUtil.ptff2(vehicleTopconfig, vehicleModel.getDeviceId());
            aa11.add(vehicleTopconfig);*/

            //生成完车型名称后，再放到集合里
            VehicleModelDto vehicleModelDto = new VehicleModelDto();
            VehicleCategory vehicleCategory = new VehicleCategory();
            VehInfoDto vehInfoDto = BeanUtil.copyProperties(vehicleModel, VehInfoDto.class);
            vehicleModelDto.setVehInfoDto(vehInfoDto);
/*            vehicleModelDto.setVehicleEngine(vehicleEngine);
            vehicleModelDto.setVehicleFull(vehicleFull);
            vehicleModelDto.setVehicleCab(vehicleCab);
            vehicleModelDto.setVehicleGearbox(vehicleGearbox);
            vehicleModelDto.setVehicleTyre(vehicleTyre);
            vehicleModelDto.setVehicleFuel(vehicleFuel);
            vehicleModelDto.setVehicleChassis(vehicleChassis);
            vehicleModelDto.setVehicleInconfig(vehicleInconfig);
            vehicleModelDto.setVehicleCell(vehicleCell);
            vehicleModelDto.setVehicleTopconfig(vehicleTopconfig);*/

            vehicleCategory.setCategoryName(vehicleModel.getCategoryName());

            String vehicleModelName = vehicleModel.createVehicleModelName(vehicleModelDto, vehicleCategory);
            vehicleModel.setDeviceName(vehicleModelName);
            aa.add(vehicleModel);
        }

        //批量存储
        //基本信息
        abcMapper.saveBatchModel(aa);
/*        //发动机
        abcMapper.saveBatchModel2(aa2);
        //整车参数
        abcMapper.saveBatchModel3(aa3);
        //驾驶室参数
        abcMapper.saveBatchModel4(aa4);
        //变速箱
        abcMapper.saveBatchModel5(aa5);
        //轮胎
        abcMapper.saveBatchModel6(aa6);
        //油箱
        abcMapper.saveBatchModel7(aa7);
        //底盘
        abcMapper.saveBatchModel8(aa8);
        //内部配置
        abcMapper.saveBatchModel9(aa9);
        //电池
        abcMapper.saveBatchModel10(aa10);
        //上装参数
        abcMapper.saveBatchModel11(aa11);*/
    }

}
