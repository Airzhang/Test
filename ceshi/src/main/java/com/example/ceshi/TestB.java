package com.example.ceshi;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import javax.lang.model.element.VariableElement;


public class TestB {

    public static void main(String[] args) {
        String s = "{\"vehInfoDto\":{\"weightType\":[1,2],\"versions\":[],\"manufacturerName\":[1,2],\"driveType\":[1],\"carBodyHeight\":[1],\"seriesId\":[1,2]},\"vehicleEngine\":{\"fuelType\":[1,2]},\"vehicleGearbox\":{},\"vehicleFull\":{},\"vehicleCab\":{},\"vehicleTyre\":{},\"vehicleFuel\":{},\"vehicleChassis\":{},\"vehicleControl\":{},\"vehicleOutconfig\":{},\"vehicleInconfig\":{},\"vehicleTopconfig\":{},\"vehicleMotor\":{\"bb\":[1,2]},\"vehicleCell\":{\"aa\":[1,2]}}";
        JSONObject jsonObject = JSONUtil.parseObj(s);
        JSONObject vehInfoDto = jsonObject.getJSONObject("vehInfoDto");
        //容器
        JSONObject js = new JSONObject();
        js.putOnce("vehicleModel", vehInfoDto);
        //去除基本信息
        jsonObject.remove("vehInfoDto");
        //创建容器
        StringBuilder ss = new StringBuilder();
        //拼接首括号
        ss.append("{");
        //遍历取出参配数据拼接字符串
        jsonObject.forEach((key,val) ->{
            String sval = val.toString();
            String replace = sval.replace("{", "").replace("}", "");
            if(!replace.equals("")){
                ss.append(replace);
                ss.append(",");
            }
        });
        //拼接尾括号
        StringBuilder ss2 = ss.deleteCharAt(ss.length() - 1);
        ss2.append("}");
        //转成JsonObject
        JSONObject canpei = JSONUtil.parseObj(ss2);
        //放入
        js.putOnce("canpei", canpei);


        System.out.println(js);

    }
}
