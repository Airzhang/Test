package com.example.ceshi;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.math.BigDecimal;

/**
 * @Author zhangxin
 * @Date 2022/5/20 12:42
 */
public class A {
    public static void main(String[] args) {
        int n = 00000000000000000000000000001011;
        int ret = 0;
        String s = String.valueOf(n);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                ret++;
            }
        }
        System.out.println(ret);
    }
}
