package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@MapperScan("com.example.carmodel")
@SpringBootApplication
public class CeshiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CeshiApplication.class, args);
    }

}
